
$.ajax({
    url: location.origin + '/chksession',
    type: 'get',
}).done(function(data) {
    msg = JSON.parse(data)
    $('#elo-login-ind').html(msg.message);
    $('#elo-login-ind').show();
    $('#elo-btn-logout').show();
    if (msg.is_admin == true) {
    $('#elo-btn-acp').show();    
    }
}).fail(function() {
    $('#elo-login-form').show();
 //   $('#elo-page').html("Error!");
});

$.ajax({
    url: location.origin + '/menus',
    type: 'get',
}).done(function(data) {
console.log(data)
obj = JSON.parse(data)
for (var i in obj) {
$(".nav").prepend('<li class="nav-item"><a class="nav-link " href="'+ obj[i].href +'">'+ obj[i].text +'</a></li>')
}
}).fail(function() {
console.log("err")
});

function login_submit() {
    var client = {
        username: $("#login_name").val(),
        secret: $("#login_secret").val()
        //      phone:$("#id-phone").val()
    }
    $('#targetlogin').html('wait...');
    // console.log(client)
    $.ajax({
        url: location.origin + '/login',
        type: 'post',
        data: JSON.stringify(client),
        //dataType: 'json',
        contentType: 'application/json; charset=utf-8',
    }).done(function(data) {
         location.reload();
      $('#targetlogin').html("Data valid, reloading");
    }).fail(function(data) {
        jmsg = JSON.parse(data.responseText);
        $('#targetlogin').html(jmsg.message);
    });
}

function logout_submit() {
    $.ajax({
        url: location.origin + '/logout',
        type: 'get',
    }).done(function(data) {
        location.reload();
    }).fail(function() {
        alert("Trouble signing off! Try again later.");
    });
}