$( document ).on( "click", ".form-inline .itemdel", function() {
    var myitemid = $(this).data('id')
    $("#delete").val(myitemid);
    $("#itemidtext").html(myitemid);

});
$.ajax({
    url: location.origin + '/chksession',
    type: 'get',
}).done(function(data) {
    msg = JSON.parse(data)
    $('#elo-login-ind').html(msg.message);
    $('#elo-login-ind').show();
    $('#elo-btn-logout').show();
    if (msg.is_admin == true) {
    $('#elo-btn-acp').show();    
    }
}).fail(function() {
    $('#elo-login-form').show();
 //   $('#elo-page').html("Error!");
});

function logout_submit() {
    $.ajax({
        url: location.origin + '/logout',
        type: 'get',
    }).done(function(data) {
        location.reload();
    }).fail(function() {
        alert("Trouble signing off! Try again later.");
    });
}

// Quill options
    Quill.register("modules/htmlEditButton", htmlEditButton);
    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'], // toggled buttons
        ['blockquote', 'code-block'],
    
        [{
            'header': 1
        }, {
            'header': 2
        }], // custom button values
        [{
            'list': 'ordered'
        }, {
            'list': 'bullet'
        }],
        [{
            'script': 'sub'
        }, {
            'script': 'super'
        }], // superscript/subscript
        [{
            'indent': '-1'
        }, {
            'indent': '+1'
        }], // outdent/indent
        [{
            'direction': 'rtl'
        }], // text direction
    
        [{
            'size': ['small', false, 'large', 'huge']
        }], // custom dropdown
        [{
            'header': [1, 2, 3, 4, 5, 6, false]
        }],
    
        [{
            'color': []
        }, {
            'background': []
        }], // dropdown with defaults from theme
        [{
            'font': []
        }],
        [{
            'align': []
        }],
    
        ['clean'], // remove formatting button
        ['link', 'image', 'video'],
        ['code-block'],
    ];


function editpage_submit() {
   pageid= $("#elo-edit-id").val()
   permission = $("input:checked").val()
    var site = {
        id: parseInt(pageid),
        pagetitle: $("#elo-edit-title").val(),
        pagepath: $("#elo-edit-path").val(),
        permission: parseInt(permission),
        content: quill.root.innerHTML,
        //      phone:$("#id-phone").val()
    }
    $('#targetsite').html('sending..');
    // console.log(client)
    reqtype=""
    if (location.search == "") {reqtype="put"} else {reqtype="post"}
    $.ajax({
        url: location.href,
        type: reqtype,
        data: JSON.stringify(site),
        dataType: 'json',
        contentType: 'application/json',
    }).done(function(data) {
        $('#targetsite').html(data.message);
    }).fail(function() {
        $('#targetsite').html("Error!");
    });
}

function editmenu_submit() {
    menuid= $("#elo-edit-id").val()
    parent_id = $("#elo-edit-parentid").val()
    permission = $("input:checked").val()
     var menu = {
         id: parseInt(menuid),
         parentid: parseInt(parent_id),
         text: $("#elo-edit-label").val(),
         href: $("#elo-edit-path").val(),
         permission: parseInt(permission),
     }
     $('#targetmenu').html('sending..');
     // console.log(client)
     reqtype=""
     if (location.search == "") {reqtype="put"} else {reqtype="post"}
     $.ajax({
         url: location.href,
         type: reqtype,
         data: JSON.stringify(menu),
         dataType: 'json',
         contentType: 'application/json',
     }).done(function(data) {
         $('#targetmenu').html(data.message);
     }).fail(function() {
         $('#targetmenu').html("Error!");
     });
 }

function edituser_submit() {
    userid= $("#elo-edit-userid").val()
    phonenum= $("#elo-edit-phone").val()
    roleval = $("input:checked").val()
     var user = {
         id: parseInt(userid),
         username: $("#elo-edit-username").val(),
         fullname: $("#elo-edit-fullname").val(),
         role: parseInt(roleval),
         email: $("#elo-edit-email").val(),
         phone: parseInt(phonenum),
         secret: $("#elo-edit-password").val(),
         //      phone:$("#id-phone").val()
     }
     $('#targetuser').html('sending..');
     // console.log(client)
     reqtype=""
     if (location.search == "") {reqtype="put"} else {reqtype="post"}
     $.ajax({
         url: location.href,
         type: reqtype,
         data: JSON.stringify(user),
         dataType: 'json',
         contentType: 'application/json',
     }).done(function(data) {
         $('#targetuser').html(data.message);
     }).fail(function() {
         $('#targetuser').html("Error!");
     });
 }

function delete_confirm(itemid) {
    var user = { id: parseInt(itemid)}
    $.ajax({
        url: location.href + "/edit",
        type: "delete",
        data: JSON.stringify(user),
        dataType: 'json',
        contentType: 'application/json',
    }).done(function(data) {
        $("#elo-message").modal('show');
        $('.modal-body').html(data.message);
    }).fail(function() {
        $("#elo-message").modal('show');
        $('.modal-body').html("Error!");
    });
}