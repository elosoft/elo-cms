package main

import (
	"eloapi/api/cms"
	dbapi "eloapi/api/dbapi"
	"fmt"
	"html/template"
	"log"
	"net/http"

	"eloapi/api/sessions"
	"eloapi/api/users"

	"github.com/gorilla/mux"
)

func main() {

	db := dbapi.DBInit()
	sessions.CookieStoreInit(db)

	defer db.Close()
	r := mux.NewRouter()

	r.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {

		if sessions.PermissionCheck(r, 4) {
			fmt.Fprintf(w, "hello from admin area")
		} else {
			fmt.Fprintf(w, "hello")
		}
	})
	// Page fcn serving (cms)
	/* 	r.HandleFunc("/page_amend", cms.Amend)

	   	r.HandleFunc("/page_create", cms.Create) */

	r.HandleFunc("/page", cms.PageServeByID)
	r.HandleFunc("/page/{path}", cms.PageServeByPath)
	r.HandleFunc("/upload", cms.UploadFile)
	// Menu fcn handling

	r.HandleFunc("/menu/create", cms.CreateMenuEntry)
	r.HandleFunc("/allmenus", cms.GetAllMenuEntries)
	r.HandleFunc("/menusbyrole", cms.GetMenuEntriesByRole)
	r.HandleFunc("/menus", cms.GetPermittedMenuEntries)
	// Blob fcn serving (cms)
	r.HandleFunc("/blobs/{id}", cms.BlobServe)
	// Client fcn serving
	r.HandleFunc("/users/create", users.Create)
	// Session handling fcns
	r.HandleFunc("/login", sessions.Login)
	r.HandleFunc("/logout", sessions.Logout)
	r.HandleFunc("/chksession", sessions.CheckSessionState)

	r.PathPrefix("/static/").
		Handler(http.StripPrefix("/static", http.FileServer(http.Dir("./website/static"))))

	sub_acp := r.PathPrefix("/admincp").Subrouter()

	// Following, in fact, routes "/admincp"
	sub_acp.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		tmpl := template.Must(template.ParseFiles("./website/layout_acp.html"))
		if sessions.PermissionCheck(r, 4) {
			tmpl.Execute(w, nil)
			return
		}
		http.Redirect(w, r, "/page/index", 403)
	})

	sub_acp.HandleFunc("/{page:(?:pages|users|menus)}", cms.AdminCPHandler)

	sub_acp.HandleFunc("/{page:(?:pages|users|menus)}/edit", cms.AdminCPEditHandler)

	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/page/index", 301)
	})

	log.Fatal(http.ListenAndServe(":8080", r))
}
