module eloapi

require (
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/wader/gormstore v0.0.0-20200328121358-65a111a20c23
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de
)
