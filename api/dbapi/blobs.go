package dbapi

import (
	"time"

	t "eloapi/api/types"
)

// StoreBlob is taking byte array from HTTP request and storing it on database
func StoreBlob(content []byte) int64 {
	blob := t.Blobs{CreationDate: time.Now().Format(elotdformat),
		ModifyDate: time.Now().Format(elotdformat),
		Content:    content}

	db.Create(&blob)
	return blob.ID
}

// GetBlob is returning requested blob (attachment)
func GetBlob(blid int64) *t.Blobs {
	input := &t.Blobs{ID: blid}

	db.First(&input)

	if input.ID != 0 {
		return input
	}
	return nil

}
