package dbapi

import (
	"log"
	"time"

	t "eloapi/api/types"
)

// CreateUser will return new ID if creation has been successful
func CreateUser(data t.User) int64 {
	//fmt.Println(data.Username)
	check, _ := QueryUser(data.Username)
	log.Printf("DBAPI[CreateUser]: CreateUser check returned ID %d", data.ID)
	if check {
		log.Printf("\nUser %s already exists", data.Username)
	} else {
		data.CreationDate = time.Now().Format(elotdformat)
		//log.Printf("DBAPI: New user password: %s", data.Secret)
		phash, _ := HashPassword(data.Secret)

		data.Secret = phash
		db.Create(&data)
	}
	return data.ID
}

// QueryUser returns true if Username has been found, and false if not
func QueryUser(Username string) (bool, *t.User) {
	input := &t.User{Username: Username}
	db.Find(&input, "username = ?", Username)
	log.Printf("DBAPI[QueryUser]: Returned username %s, with ID %d ", input.Username, input.ID)
	if input.ID != 0 {
		return true, input
	}
	return false, nil
}

// GetUserByID is returning user object by given user ID
func GetUserByID(userid int64) *t.User {
	input := &t.User{ID: userid}
	db.Find(&input, "id = ?", userid)
	log.Printf("DBAPI[GetUserByID]: For ID %d username is %s", userid, input.Username)
	if input.ID != 0 {
		return input
	}
	return nil
}

// GetRole is returning user role by given user ID
func GetRole(id int64) int {
	input := &t.User{ID: id}
	db.Find(&input, "id = ?", id)
	log.Printf("DBAPI[GetRole]: Role for user ID %d is %d", input.ID, input.Role)
	return input.Role
}

// GetAllUsers is returning all users from db users table
func GetAllUsers() *[]t.User {
	data := &[]t.User{}

	db.Select("id,username,company_id,role").Find(&data)

	return data
}

// AlterUser sets cursor of desired User and updates with provided set
func AlterUser(data t.User) bool {

	qresult, cursor := QueryUser(data.Username)

	if qresult == false {
		return false
	}

	phash, _ := HashPassword(data.Secret)

	data.Secret = phash

	db.Debug().Model(&cursor).Updates(data)

	return true
}

// DeleteUser deletes user entry from db
func DeleteUser(data t.User) bool {
	input := t.User{ID: data.ID}

	db.First(&input)
	if input.ID == 0 {
		return false
	}
	db.Debug().Unscoped().Delete(&input)
	return true

}
