package dbapi

import (
	"log"

	t "eloapi/api/types"
)

// QueryMenuEntry to check if exist
func QueryMenuEntry(input t.MenuEntries) int64 {
	db.Find(&input, "ID = ?", input.ID)
	//log.Printf("result: %d", input.ID)

	return input.ID
}

// GetMenuEntryByID returns menu object by given ID
func GetMenuEntryByID(id int64) *t.MenuEntries {
	input := &t.MenuEntries{ID: id}

	db.First(&input)
	if input.ID == 0 {
		return nil
	}
	log.Printf("DBAPI[GetMenuEntryByID]: Returned %d", input.ID)
	return input
}

// GetAllMenuEntries returning all menu entries in DB
func GetAllMenuEntries() []t.MenuEntries {
	data := []t.MenuEntries{}
	db.Find(&data)
	//	fmt.Println(data)
	return data
}

// GetMenuEntriesByRole returning per-role menu entries
func GetMenuEntriesByRole(role int) []t.MenuEntries {
	data := []t.MenuEntries{}
	db.Where("permission = ?", role).Find(&data)
	//	fmt.Println(data)
	return data
}

// GetPermittedMenuEntries returns public menu entries and these who are permitted to logged in user
func GetPermittedMenuEntries(role int) []t.MenuEntries {
	data := []t.MenuEntries{}
	if role != 0 {
		db.Where("permission IN (?)", []int{1, role}).Find(&data) // logged in
	} else {
		db.Where("permission = ?", 1).Find(&data) // public entries
	}
	return data
}

// CreateMenuEntry to insert data to db
func CreateMenuEntry(data t.MenuEntries) int64 {

	id := QueryMenuEntry(data)
	if id == 0 {
		db.Create(&data)
		return data.ID
	}
	return 0
}

// AlterMenuEntry is modifying menu entry data using provided data
func AlterMenuEntry(data t.MenuEntries) bool {

	input := t.MenuEntries{ID: data.ID}
	db.First(&input)

	if input.ID == 0 {
		return false
	}
	db.Debug().Model(&data).Updates(data)
	return true
}

// DeleteMenuEntry to delete data from db
func DeleteMenuEntry(data t.MenuEntries) bool {
	input := t.MenuEntries{ID: data.ID}

	db.First(&input)
	if input.ID == 0 {
		return false
	}
	db.Debug().Unscoped().Delete(&input)
	return true

}
