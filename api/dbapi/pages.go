package dbapi

import (
	"log"
	"time"

	t "eloapi/api/types"
)

// QueryPage checks if we're trying to use same path as Page that already is in DB
func QueryPage(input *t.Pages) int64 {
	db.Find(&input, "Path = ?", input.Path)
	log.Printf("DBAPI[QueryPage]: result is %s", input.Path)

	return input.ID
}

// CreatePage is creating new entry in Pages to modify it afterwards
func CreatePage(data t.Pages) int64 {
	input := &t.Pages{Path: data.Path}

	id := QueryPage(input)

	if id == 0 {
		db.Create(&data)
		return data.ID
	}

	return 0
}

// AlterPage is modifying Page data using provided data
func AlterPage(data t.Pages) bool {

	input := t.Pages{Path: data.Path}
	db.First(&input)

	if input.ID == 0 {
		return false
	}
	data.ModifyDate = time.Now().Format(elotdformat)
	db.Debug().Model(&data).Updates(data)
	return true
}

// DeletePage is deleting entry in Pages
func DeletePage(data t.Pages) bool {
	input := t.Pages{ID: data.ID}

	db.First(&input)
	if input.ID == 0 {
		return false
	}
	db.Debug().Unscoped().Delete(&input)
	return true

}

// GetAllPages returns all entries from Page table
func GetAllPages() *[]t.Pages {

	data := &[]t.Pages{}

	db.Select("id,path,title,permission").Find(&data)

	return data
}

// GetPageByID returns Page object by given ID
func GetPageByID(id int64) *t.Pages {
	input := &t.Pages{ID: id}

	db.First(&input)
	if input.ID == 0 {
		return nil
	}
	return input
}

// GetPageByPath returns Page object by given Path
func GetPageByPath(path string) *t.Pages {
	input := &t.Pages{}

	//	db.First(&input)
	db.Where("path = ?", path).First(&input)
	log.Printf("DBAPI[GetPageByPath]: Found ID %d for Path %s", input.ID, path)
	if input.ID == 0 {
		return nil
	}
	return input
}
