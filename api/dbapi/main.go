package dbapi

import (
	"fmt"
	"log"

	// "github.com/gin-gonic/gin"

	t "eloapi/api/types"

	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3" // Dialect for sqlite3
	"golang.org/x/crypto/bcrypt"
)

var db *gorm.DB
var err error

const elotdformat = "02-01-2006 15:04:05 -0700"

// HashPassword helper to convert password to bcrypt so it won't be saved as plaintext
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// DBInit initialises database and returns instance (needed on main package for handling init and close db when it stops)
func DBInit() *gorm.DB {
	db, err = gorm.Open("sqlite3", "./db/elodb.sqlite")
	if err != nil {
		fmt.Println(err)
	}
	//	defer db.Close()

	// Each type has to be migrated to db to create/alter appropriate structure
	db.Debug().AutoMigrate(&t.User{})
	db.Debug().AutoMigrate(&t.Invoices{})
	db.Debug().AutoMigrate(&t.Company{})
	db.Debug().AutoMigrate(&t.Payments{})
	db.Debug().AutoMigrate(&t.Pages{})
	db.Debug().AutoMigrate(&t.Blobs{})
	db.Debug().AutoMigrate(&t.MenuEntries{})

	log.Println("DB Initialised.")

	return db
}

/*
//	id := CreateClient("jojo", "jojo@jojo.com")
//	fmt.Printf("\nCreateClient returned id: %v\n", id)
	siteresult := CreateSite(`/test/dupa`)
	fmt.Printf("\nCreateSite returned: %v \n", siteresult)
	res := AlterSite(`/test/dupa`,`<b>dupa</b>`)
	fmt.Printf("AlterSite returned %v", res)

	cl := t.Client{}

	cl.Username = "jojo"
cl.Fullname = "Joseph Joestar"
	cl.Companyname = "jojo ltd"

//	cl.Email = "dupa@dupa.pl"

	cl.Phone = 48123123123

	re := AlterClient(cl)

	fmt.Printf("\nAlterclient returned: %v\n",re)
	 test examples
	cc := &Client{Username: "elo", Email: "elo@elo.com", CreationDate: time.Now()}
	ci := &Invoices{ID
: 1, CreationDate: time.Now(), Paid: true, Value: 2.137, Currency: "EUR"}
	co := &Operator{Iduser: 1, Username: "test", Secret: "dupa", Fullname: "Test",  CreationDate: time.Now()}
	cp := &Payments{Idpayment: 1, ID
: 1, Idinvoice: 1, Date: time.Now()}
	cs := &Sites{ID: 1, Path: "test", CreationDate: time.Now(), ModifyDate: time.Now()}
	db.Create(&cc)
	db.Create(&ci)
	db.Create(&co)
	db.Create(&cp)
	db.Create(&cs)
*/

//}
