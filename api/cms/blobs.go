package cms

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"eloapi/api/dbapi"

	"eloapi/api/sessions"

	"eloapi/api/utils"

	"github.com/gorilla/mux"
)

// UploadFile hanler for uploading blob into DB
func UploadFile(w http.ResponseWriter, r *http.Request) {
	if !sessions.PermissionCheck(r, 4) {
		w.WriteHeader(http.StatusForbidden)
	} else {
		fmt.Println("File Upload Endpoint Hit")

		// Parse our multipart form, 10 << 20 specifies a maximum
		// upload of 10 MB files.
		r.ParseMultipartForm(10 << 20)
		// FormFile returns the first file for the given key `myFile`
		// it also returns the FileHeader so we can get the Filename,
		// the Header and the size of the file
		file, handler, err := r.FormFile("myFile")
		if err != nil {
			log.Println("Error Retrieving the File")
			log.Println(err)
			return
		}
		defer file.Close()
		log.Printf("Uploaded File: %+v\n", handler.Filename)
		log.Printf("File Size: %+v\n", handler.Size)
		log.Printf("MIME Header: %+v\n", handler.Header)

		// read all of the contents of our uploaded file into a
		// byte array
		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			fmt.Println(err)
		}
		blobid := dbapi.StoreBlob(fileBytes)
		// return that we have successfully uploaded our file!
		fmt.Fprintf(w, "Successfully Uploaded File with id: %d\n", blobid)
	}
}

// BlobServe handler to serve blob by given id
func BlobServe(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	blobid := vars["id"]
	blobidInt, _ := strconv.Atoi(blobid)
	blob := dbapi.GetBlob(int64(blobidInt))

	perm := blob.Permission

	if sessions.PermissionCheck(r, perm) || perm == 0 {
		w.Write(blob.Content)
		return
	}
	w.WriteHeader(http.StatusForbidden)
	w.Write(utils.FormMessage("Forbidden"))
	//	w.Write(contents)
}
