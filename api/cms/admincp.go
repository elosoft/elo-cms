package cms

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"text/template"

	"eloapi/api/dbapi"
	t "eloapi/api/types"

	"eloapi/api/sessions"

	"eloapi/api/utils"

	"github.com/gorilla/mux"
)

// AdminCPHandler is serving data from db columns by type after it hit one of pagesel routes
func AdminCPHandler(w http.ResponseWriter, r *http.Request) {
	if !sessions.PermissionCheck(r, 4) {
		w.WriteHeader(http.StatusForbidden)
	} else {
		vars := mux.Vars(r)
		log.Printf("CMS[AdminCPHandler]: Received route to %s", vars["page"])
		tmpl := template.Must(template.ParseFiles("./website/layout_acp.html"))

		switch pagesel := vars["page"]; pagesel {

		case "pages":
			data := dbapi.GetAllPages() // In fact, this does not include site content - don't need that in table

			err := tmpl.Execute(w, map[string]interface{}{
				"PagesData": data,
			})
			if err != nil {
				log.Println(err)
			}
			//		w.Write(data)
		case "menus":
			data := dbapi.GetAllMenuEntries() // In fact, this does not include site content - don't need that in table

			err := tmpl.Execute(w, map[string]interface{}{
				"MenusData": data,
			})
			if err != nil {
				log.Println(err)
			}
		case "users":
			data := dbapi.GetAllUsers() // In fact, this does not include site content - don't need that in table

			err := tmpl.Execute(w, map[string]interface{}{
				"UsersData": data,
			})
			if err != nil {
				log.Println(err)
			}
		}
	}
}

// AdminCPEditHandler is performing Create/Amend/Delete operations
// basing on request method.
// POST - Amend
// PUT - Create
// DELETE - Delete
// it has switch pagesel to determine on which type of item
// we want to perform the operation.
func AdminCPEditHandler(w http.ResponseWriter, r *http.Request) {
	if !sessions.PermissionCheck(r, 4) {
		w.WriteHeader(http.StatusForbidden)
	} else {
		var idparam []string
		var itemid string
		var itemidInt int
		vars := mux.Vars(r)
		log.Printf("CMS[AdminCPEditHandler]: Received route to %s", vars["page"])
		tmpl := template.Must(template.ParseFiles("./website/layout_acp_edit.html"))
		if r.Method != "DELETE" {
			idparam, _ = r.URL.Query()["id"]
			if len(idparam) != 0 {
				itemid = idparam[0]
				log.Println(itemid)
				itemidInt, _ = strconv.Atoi(itemid)
			}
		}
		switch pagesel := vars["page"]; pagesel {

		case "pages":
			var err error
			if r.Method == "GET" {
				if itemid != "" {
					data := dbapi.GetPageByID(int64(itemidInt))

					err = tmpl.Execute(w, map[string]interface{}{
						"PageData": data,
					})
				} else {
					err = tmpl.Execute(w, map[string]interface{}{
						"PageData": t.Pages{},
					})
				}
				if err != nil {
					log.Println(err)
				}
			} else if r.Method == "POST" {
				page := t.Pages{}
				json.NewDecoder(r.Body).Decode(&page)
				if page.ID != 0 {
					if dbapi.AlterPage(page) {
						w.Write(utils.FormMessage("Page updated."))
						return
					}
					w.WriteHeader(http.StatusInternalServerError)
					w.Write(utils.FormMessage("Problem updating page!"))
					return
				}
			} else if r.Method == "PUT" {
				page := t.Pages{}
				json.NewDecoder(r.Body).Decode(&page)
				if dbapi.CreatePage(page) != 0 {
					w.Write(utils.FormMessage("Page created"))
					return
				}
				w.WriteHeader(http.StatusInternalServerError)
				w.Write(utils.FormMessage("Problem creating page!"))
				return
			} else if r.Method == "DELETE" {
				page := t.Pages{}
				json.NewDecoder(r.Body).Decode(&page)
				if dbapi.DeletePage(page) {
					w.Write(utils.FormMessage("Page deleted"))
					return
				}
				w.WriteHeader(http.StatusInternalServerError)
				w.Write(utils.FormMessage("Problem deleting page!"))
				return
			}
		case "users":
			if r.Method == "GET" {
				var err error
				if itemid != "" {
					data := dbapi.GetUserByID(int64(itemidInt))
					err = tmpl.Execute(w, map[string]interface{}{
						"UserData": data,
					})
				} else {
					err = tmpl.Execute(w, map[string]interface{}{
						"UserData": t.User{},
					})
				}
				if err != nil {
					log.Println(err)
				}
			} else if r.Method == "POST" {
				user := t.User{}
				json.NewDecoder(r.Body).Decode(&user)
				if user.ID != 0 {
					if dbapi.AlterUser(user) {
						w.Write(utils.FormMessage("User updated."))
						return
					}
					w.WriteHeader(http.StatusInternalServerError)
					w.Write(utils.FormMessage("Problem updating user!"))
					return
				}
			} else if r.Method == "PUT" {
				user := t.User{}
				json.NewDecoder(r.Body).Decode(&user)
				if dbapi.CreateUser(user) != 0 {
					w.Write(utils.FormMessage("User created"))
					return
				}
				w.WriteHeader(http.StatusInternalServerError)
				w.Write(utils.FormMessage("Problem creating user!"))
				return
			} else if r.Method == "DELETE" {
				user := t.User{}
				json.NewDecoder(r.Body).Decode(&user)
				if dbapi.DeleteUser(user) {
					w.Write(utils.FormMessage("User deleted"))
					return
				}
				w.WriteHeader(http.StatusInternalServerError)
				w.Write(utils.FormMessage("Problem deleting user!"))
				return
			}
		case "menus":
			if r.Method == "GET" {
				var err error
				if itemid != "" {
					data := dbapi.GetMenuEntryByID(int64(itemidInt))
					err = tmpl.Execute(w, map[string]interface{}{
						"MenusData": data,
					})
				} else {
					err = tmpl.Execute(w, map[string]interface{}{
						"MenusData": t.MenuEntries{},
					})
				}
				if err != nil {
					log.Println(err)
				}
			} else if r.Method == "POST" {
				entry := t.MenuEntries{}
				json.NewDecoder(r.Body).Decode(&entry)
				if entry.ID != 0 {
					if dbapi.AlterMenuEntry(entry) {
						w.Write(utils.FormMessage("Menu entry updated."))
						return
					}
					w.WriteHeader(http.StatusInternalServerError)
					w.Write(utils.FormMessage("Problem updating menu entry!"))
					return
				}
			} else if r.Method == "PUT" {
				entry := t.MenuEntries{}
				json.NewDecoder(r.Body).Decode(&entry)
				if dbapi.CreateMenuEntry(entry) != 0 {
					w.Write(utils.FormMessage("Menu entry created"))
					return
				}
				w.WriteHeader(http.StatusInternalServerError)
				w.Write(utils.FormMessage("Problem creating menu entry !"))
				return
			} else if r.Method == "DELETE" {
				entry := t.MenuEntries{}
				json.NewDecoder(r.Body).Decode(&entry)
				if dbapi.DeleteMenuEntry(entry) {
					w.Write(utils.FormMessage("Menu entry deleted"))
					return
				}
				w.WriteHeader(http.StatusInternalServerError)
				w.Write(utils.FormMessage("Problem deleting menu entry!"))
				return
			}
		}

	}
}
