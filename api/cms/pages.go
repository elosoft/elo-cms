package cms

import (
	"eloapi/api/dbapi"
	t "eloapi/api/types"
	"log"
	"net/http"
	"strconv"
	"text/template"

	"eloapi/api/sessions"

	"eloapi/api/utils"

	"github.com/gorilla/mux"
)

const ()

// PageServeByID is handler for "/page?id={id}", serving pages regarding of "id" column in db "pages" table
func PageServeByID(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("./website/layout.html"))

	pageids, _ := r.URL.Query()["id"]
	pageid := pageids[0]
	log.Println(pageids)
	pageidInt, _ := strconv.Atoi(pageid)
	page := dbapi.GetPageByID(int64(pageidInt))

	log.Printf("DBAPI[PageServeByID]: Returned Page with ID: %d", page.ID)
	perm := page.Permission

	if sessions.PermissionCheck(r, perm) || perm == 1 {
		tmpl.Execute(w, page)
		return
	}
	ForbiddenPage := t.Pages{Content: "You don't have permission to access this page."}
	w.WriteHeader(http.StatusForbidden)
	tmpl.Execute(w, ForbiddenPage)
	//fmt.Fprintf(w, page.Content)

}

// PageServeByPath is handler for "/page/", serving pages regarding of "path" column in db "pages" table
// so pages can be accessed if it match "/page/{path from request}"
func PageServeByPath(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("./website/layout.html"))
	vars := mux.Vars(r)
	pagepath := vars["path"]
	log.Printf("CMS[pageServeByPath]: Took path %s", pagepath)
	page := dbapi.GetPageByPath(pagepath)
	if page != nil {
		perm := page.Permission

		if sessions.PermissionCheck(r, perm) || perm == 1 {
			tmpl.Execute(w, page)
			return
		}

		ForbiddenPage := t.Pages{Content: "You don't have permission to access this page."}
		w.WriteHeader(http.StatusForbidden)
		tmpl.Execute(w, ForbiddenPage)
	} else {
		w.WriteHeader(http.StatusNotFound)
		w.Write(utils.FormMessage("Requested webpage has not been found."))
	}
}
