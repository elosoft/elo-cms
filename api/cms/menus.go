package cms

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"eloapi/api/dbapi"
	t "eloapi/api/types"

	"eloapi/api/sessions"

	"eloapi/api/utils"
)

// GetAllMenuEntries returns all menu entries from db "menus" table, this is limited only to AdminCP
// so can be displayed.
func GetAllMenuEntries(w http.ResponseWriter, r *http.Request) {
	if !sessions.IsActive(r) {
		w.WriteHeader(http.StatusForbidden)
		w.Write(utils.FormMessage("Forbidden"))
	}
	if !sessions.PermissionCheck(r, 4) {
		w.WriteHeader(http.StatusForbidden)
	} else {
		data := dbapi.GetAllMenuEntries()
		fmt.Println(data)
		json.NewEncoder(w).Encode(data)
	}
}

// GetMenuEntriesByRole is helper for GetPermittedMenuEntries to return entries
// regarding of logged user role.
func GetMenuEntriesByRole(w http.ResponseWriter, r *http.Request) {
	role := sessions.ReturnRole(r)
	data := dbapi.GetMenuEntriesByRole(role)
	json.NewEncoder(w).Encode(data)
}

// GetPermittedMenuEntries is returning menu entries that logged user is permitted to see
func GetPermittedMenuEntries(w http.ResponseWriter, r *http.Request) {
	if !sessions.IsActive(r) {
		data := dbapi.GetMenuEntriesByRole(1)
		json.NewEncoder(w).Encode(data)
		return
	}
	role := sessions.ReturnRole(r)
	data := dbapi.GetPermittedMenuEntries(role)
	json.NewEncoder(w).Encode(data)
}

// CreateMenuEntry is creating entry in DB if request is properly formed
func CreateMenuEntry(w http.ResponseWriter, r *http.Request) {
	if !sessions.PermissionCheck(r, 4) {
		w.WriteHeader(http.StatusForbidden)
	} else {
		var resp []byte
		entry := t.MenuEntries{}

		request := func() {
			result := dbapi.CreateMenuEntry(entry)

			if result != 0 {
				resp = utils.FormMessage("Entry created with ID " + strconv.FormatInt(result, 10))
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				resp = utils.FormMessage("Error!")
			}
		}

		err := json.NewDecoder(r.Body).Decode(&entry)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			resp = utils.FormMessage("Error decoding JSON!")
		} else {
			fmt.Println(entry.Text)
			fmt.Println(entry.Permission)

			switch permission := entry.Permission; permission {
			case 0:
				w.WriteHeader(http.StatusInternalServerError)
				resp = utils.FormMessage("Permission must be set")
			case 1:
				request()
			case 2:
				request()
			case 4:
				request()
			default:
				w.WriteHeader(http.StatusInternalServerError)
				resp = utils.FormMessage("Wrong Permission")
			}
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(resp)
	}
}
