package types

// User type for holding client user accounts
type User struct {
	ID           int64  `gorm:"primary_key" json:"id"`
	Username     string `gorm:"unique;not null" json:"username"`
	FullName     string `json:"fullname,omitempty"`
	CompanyID    string `sql:"type:bigint REFERENCES company(id)" json:"companyid"`
	CreationDate string `json:"creationdate,omitempty"`
	Secret       string `json:"secret,omitempty"`
	Email        string `json:"email,omitempty"`
	Phone        int    `json:"phone,omitempty"`
	Role         int    `json:"role"`
}

// Company type for holding company data
type Company struct {
	ID           int64  `gorm:"primary_key" json:"id"`
	CompanyName  string `json:"companyname,omitempty"`
	CreationDate string `json:"creationdate,omitempty"`
	AddressData  string `json:"addressdata,omitempty"`
	Phone        int    `json:"phone,omitempty"`
}

// Invoices type for holding invoice data
type Invoices struct {
	ID           int64   `gorm:"primary_key" json:"id"`
	IDClient     int64   `sql:"type:bigint REFERENCES clients(id)" json:"idclient"`
	CreationDate string  `json:"creationdate,omitempty"`
	Data         []byte  `sql:"type:blob"`
	Paid         bool    `json:"paid,omitempty"`
	Value        float64 `json:"value,omitempty"`
	Currency     string  `sql:"type:varchar(3)" json:"currency,omitempty"`
}

// Payments type for payment-specific data
type Payments struct {
	ID           int64   `gorm:"primary_key" json:"id"`
	IDClient     int64   `sql:"type:bigint REFERENCES clients(id)" json:"idclient"`
	IDInvoice    int64   `sql:"type:bigint REFERENCES invoices(id)" json:"idinvoice"`
	Date         string  `json:"date,omitempty"`
	Type         string  `json:"type,omitempty"`
	Provider     string  `json:"provider,omitempty"`
	AmountPaid   float64 `json:"amountpaid,omitempty"`
	CurrencyPaid string  `sql:"type:varchar(3)" json:"currencypaid,omitempty"`
	AmountOrig   float64 `json:"amountorig,omitempty"`
	CurrencyOrig float64 `sql:"type:varchar(3)" json:"currencyorig,omitempty"`
	ExchangeRate float64 `json:"exchangerate,omitempty"`
}

// Pages type for inner-company CMS Pages
type Pages struct {
	ID           int64  `gorm:"primary_key" json:"id"`
	Path         string `gorm:"unique;not null" json:"pagepath,omitempty"`
	Title        string `json:"pagetitle,omitempty"`
	CreationDate string `json:"creationdate,omitempty"`
	ModifyDate   string `json:"modifydate,omitempty"`
	MetaTags     string `sql:"type:text" json:"Pagemetatags,omitempty"`
	Content      string `sql:"type:text" json:"content,omitempty"`
	Permission   int    `json:"permission"` // 1 - open, 2 - client, 4 - operator
}

// Menu entries shown on template, vary on permission level
type MenuEntries struct {
	ID         int64  `gorm:"primary_key" json:"id"`
	ParentID   int64  `json:"parentid,omitempty"`
	Text       string `json:"text"`
	Href       string `json:"href"`
	Permission int    `json:"permission"` // 1 - open, 2 - client, 4 - operator
}

// Blobs type for Page attachments
type Blobs struct {
	ID           int64  `gorm:"primary_key" json:"id"`
	CreationDate string `json:"creationdate,omitempty"`
	ModifyDate   string `json:"modifydate,omitempty"`
	Content      []byte `sql:"type:blob"`
	Permission   int    `json:"permission"` // 1 - open, 2 - client, 4 - operator
}
