package users

import (
	"encoding/json"
	"net/http"

	"eloapi/api/dbapi"
	t "eloapi/api/types"

	"eloapi/api/utils"
)

func Create(w http.ResponseWriter, r *http.Request) {
	var resp []byte
	User := t.User{}

	json.NewDecoder(r.Body).Decode(&User)
	// we cannot pass empty data
	if User.Username != "" && User.Secret != "" {
		iduser := dbapi.CreateUser(User)
		// CreateUser returns data with ID. 0 means that it couldn't be created
		if iduser != 0 {
			resp = utils.FormMessage("User created")
			//fmt.Fprintf(w, "User created, id: %d", idop)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			resp = utils.FormMessage("Problem creating User")
			//fmt.Fprintf(w, "Problem creating User")
		}
		// if data is empty, return error
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		resp = utils.FormMessage("Missing data")
	}
	// headers passed in loop (no header set - status 200 OK), so write response
	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}
