package sessions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"eloapi/api/dbapi"
	t "eloapi/api/types"

	"eloapi/api/utils"

	"github.com/jinzhu/gorm"
	"github.com/wader/gormstore"
	"golang.org/x/crypto/bcrypt"
)

var store *gormstore.Store // as global variable - we cannot create http handler when it has 3 methods
// CookieStoreInit takes argument db as current DB connection
func CookieStoreInit(db *gorm.DB) *gormstore.Store {
	store = gormstore.New(db, []byte("secret"))
	return store
}

func PermissionCheck(r *http.Request, role int) bool {
	session, _ := store.Get(r, "sessions")

	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		return false // Session not valid, permission denied
	}
	userid := session.Values["auth_user_id"].(int64)

	if dbapi.GetRole(userid) < role {
		return false // Role of logged in user is not equal to requested role, permission denied
	}

	return true // Roles equal to each other, grant permission

}

func ReturnRole(r *http.Request) int {
	session, _ := store.Get(r, "sessions")
	userid := session.Values["auth_user_id"].(int64)
	return dbapi.GetRole(userid)
}

// CheckPasswordHash compares password from body with one stored in DB
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func IsActive(r *http.Request) bool {
	session, _ := store.Get(r, "sessions")

	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		return false
	}
	return true
}

// CheckSessionState lookup for cookie in store and returns
func CheckSessionState(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "sessions")

	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		w.WriteHeader(http.StatusForbidden)
		w.Write(utils.FormMessage("Forbidden"))
		return
	}
	userid := session.Values["auth_user_id"]

	if dbapi.GetRole(userid.(int64)) == 4 {
		json.NewEncoder(w).Encode((map[string]interface{}{
			"message":  "Session active and your ID is: " + fmt.Sprint(userid),
			"is_admin": true,
		}))
		return
	}

	w.Write(utils.FormMessage("Session active and your ID is: " + fmt.Sprint(userid)))
}

func Login(w http.ResponseWriter, r *http.Request) {
	var resp []byte
	session, _ := store.Get(r, "sessions")

	input := t.User{}
	json.NewDecoder(r.Body).Decode(&input)

	tf, user := dbapi.QueryUser(input.Username)

	if tf {
		match := CheckPasswordHash(input.Secret, user.Secret)
		if match {
			session.Values["auth_user_id"] = user.ID
			// Set user as authenticated
			session.Values["authenticated"] = true
			//session.Options.MaxAge = 30
			session.Save(r, w)
			resp = utils.FormMessage("OK")
		} else {
			w.WriteHeader(http.StatusForbidden)
			resp = utils.FormMessage("Wrong username or password")
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		resp = utils.FormMessage("Wrong username or password")
	}

	w.Write(resp)
}

func Logout(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "sessions")

	// Revoke users authentication
	session.Values["authenticated"] = false
	session.Save(r, w)
	w.Write(utils.FormMessage("OK"))
}
