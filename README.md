# elo-bundle
This is Elo Software bundle - CMS web application served on port *2137* with corresponding API. Application data is stored in sqlite3 database.

Backend is running as Go application, frontend in Go templates using Bootstrap and JavaScript.

Application aims to be fast and efficient CMS for small businesses with invoicing and which could accept payments from various payment providers. 

Package is splitted into following parts of CMS:

## Users & Roles

There are three types of permissions (public, client and admin), and two types of roles so far (clients and admins). New users can be created from CMS by admin user. 

Default login:

admin/admin4321 (admin logon)

client/client4321 (client logon)

## Dynamic menus

CMS supports adding new menu entries, which will be displayed under logo. Each menu entry can have it's own permission, so will be accessible only for users which have sufficient permissions. Currently it does not support upper levels than first.

## Pages

There is inbuilt page management with WYSIWYG/HTML editor, which allows to set page title, permission and path. After creating page, it can be accessed by /page?id=[x] or by page/[path] where id is page id from database (shown on admin panel after being created) or by path set while creating. Currently it does not support upper levels than first.

## Blobs (attachments) - no frontend yet

Blobs can be uploaded by admin and have assigned permission as well - which prevents unauthorized users from accessing to forbidden part of cms.

## Todos

* support password-protected SQLite databases
* Invoices module 
* Payments module
* Better Look&Feel of default layout